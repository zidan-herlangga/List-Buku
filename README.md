# List Buku - Array Multidimensional


<img src="Image/Banner.png" alt="">

---

## Deskripsi
List Buku adalah judul proyek untuk tugas kuliah yang bertujuan untuk mengimplementasikan konsep-konsep ilmu Array Multidimensional menggunakan bahasa pemrograman Python.

## Penggunaan
1. Klik button <strong>Code</strong> untuk Download repositori<br>
    - Jika menggunakan git clone salin: `git clone https://github.com/zidan-herlangga/list-buku.git`

    or

    - Jika tidak menggunakan pilih `Download ZIP`

2. Jalankan script utama dengan perintah: `python main.py`

3. Ikuti petunjuk yang diberikan dalam aplikasi untuk menggunakan menu yang tersedia.

## Menu Program
<img src="Image/Menu.png">

## Kontribusi
- Zidan Herlangga 
- Gusiar Ilham
- Naufal Ralfi
- Novan Risqi
- Maulana Wiksa
